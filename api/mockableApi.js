console.log("Starting mockableApi.js");
const config = require("../config").config;
const server = require("../server.js");
const app = server.app;
const url = require("url");

const request = require('request');


app.get("/api/getRestaurantByLocation", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var locationName = query.locationName;
  getLocationCoordinates(locationName, (location) => {
    res.json({ location });
  })

});




const getLocationCoordinates = (locationName, callback) => {
  var options = {
    url: `${config.mockableUrl}location`,
  };

  request(options, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body);
      console.log(info)
      getRestaurentByCoordinate(info, callback)
    }
  });
}




const getRestaurentByCoordinate = (locationData, callback) => {
  var options = {
    url: `${config.mockableUrl}search`,
  };

  request(options, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var restaurantsInLocation = JSON.parse(body);
      console.log(restaurantsInLocation)
      let data = {
        locationData,
        restaurantsInLocation
      }
      callback(data)
    }
  });
}


