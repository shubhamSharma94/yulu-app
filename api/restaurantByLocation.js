console.log("Starting restaurantByLocation.js");
const config = require("../config").config;
const server = require("../server.js");
const app = server.app;
const url = require("url");

const request = require('request');

const zomatoKey1 = config.zomatoKey;
const zomatoKey2 = config.zomatoKey2;

app.get("/api/getRestaurantByLocation", (req, res) => {
  var url_parts = url.parse(req.url, true);
  var query = url_parts.query;
  var locationName = query.locationName;
  getLocationCoordinates(locationName, (location) => {
    res.json({ location  });
  })

});




const getLocationCoordinates = (locationName, callback) => {
  var options = {
    url: `${config.zomatoApi}locations`,
    headers: {
      'user-key': zomatoKey1
    },
    qs: {query: locationName}
  };
  // console.log(locationName)

  request(options, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var info = JSON.parse(body);
      // console.log(info)
      getRestaurentByCoordinate(info, callback)
    }
  });
}




const getRestaurentByCoordinate = (locationData, callback) => {
  let location_suggestions = locationData.location_suggestions[0]

  var options = {
    url: `${config.zomatoApi}search`,
    headers: {
      'user-key': zomatoKey1
    },

    qs: {
      entity_id: String(location_suggestions.entity_id), 
      entity_type: location_suggestions.entity_type,
      lat: location_suggestions.latitude,
      lon: location_suggestions.longitude,
    }
  };

  request(options, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      var restaurantsInLocation = JSON.parse(body);
      let data = {
        locationData,
        restaurantsInLocation
      }
      callback(data)
    }
  });
}

