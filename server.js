const express = require('express');
const app = express();
const port = process.env.PORT || 5001;
const path = require('path');


app.use(express.static(path.join(__dirname, "client/build")));

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, "client/build", "index.html"));
});

app.use(express.json());   
app.use(express.urlencoded());

app.listen(port, () => console.log(`Example app listening on port ${port}!`));


module.exports.app = app;
